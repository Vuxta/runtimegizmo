using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VuxtaStudio.GizmoRuntime.GizmoUI;

namespace VuxtaStudio.GizmoRuntime
{
    public enum GizmoMode
    {
        None,
        Move,
        Scale,
        Rotate
    }

    public enum GizmoAxis
    {
        None,
        X,
        Y,
        Z,
        XY,
        YZ,
        XZ,
        Any
    }

    public class RuntimeGizmo : MonoBehaviour
    {
        [SerializeField]
        private float m_ScaleFactor = 1f;

        [SerializeField]
        private float m_RotateSensitivity = 10f;

        [SerializeField]
        private GizmoUIBase m_TranslateGizmo;

        [SerializeField]
        private ScaleGizmoUIBase m_ScaleGizmo;

        [SerializeField]
        private GizmoUIBase m_RotateGizmo;

        private bool m_Enable = false;
        private GameObject m_TargetObject;

        private GizmoMode m_RunningGizmoMode = GizmoMode.None;
        private GizmoAxis m_RunningGizmoAxis = GizmoAxis.None;
        private bool m_TranslateOnPlane = false;
        private Ray m_CurrentPointerRay;
        private Vector3 m_OperationPlaneNormal;
        private Vector3 m_OperationOrigin;
        private Vector3 m_ProjectedAxis;
        private Vector3 m_ObjectWorkingAixs;
        private Vector3 m_MousePositionOnOperationPlane;
        private Vector3 m_PreviousMousePositionOnOperationPlane;
        private Vector3 m_TargetXAxis;
        private Vector3 m_TargetYAxis;
        private Vector3 m_TargetZAxis;

        private void Start()
        {
            SetGizmoDisable();
        }

        public void SetGizmoDisable()
        {
            m_TranslateGizmo.SetEnable(false);
            m_ScaleGizmo.SetEnable(false);
            m_RotateGizmo.SetEnable(false);
            m_RunningGizmoMode = GizmoMode.None;
            m_RunningGizmoAxis = GizmoAxis.None;
        }

        public void SetGizmoEnable(GameObject target, GizmoMode mode)
        {
            m_Enable = true;
            m_TranslateGizmo.SetEnable(false);
            m_ScaleGizmo.SetEnable(false);
            m_RotateGizmo.SetEnable(false);
            m_TargetObject = target;
            m_RunningGizmoMode = GizmoMode.None;
            m_RunningGizmoAxis = GizmoAxis.None;

            if (mode == GizmoMode.Move)
                m_TranslateGizmo.SetEnable(true);
            else if (mode == GizmoMode.Scale)
                m_ScaleGizmo.SetEnable(true);
            else if (mode == GizmoMode.Rotate)
                m_RotateGizmo.SetEnable(true);
        }

        public void SetHover(RaycastHit hitInfo)
        {
            m_RunningGizmoMode = GizmoMode.None;
            m_RunningGizmoAxis = GizmoAxis.None;

            m_TranslateGizmo.SetHover(hitInfo);
            m_ScaleGizmo.SetHover(hitInfo);
            m_RotateGizmo.SetHover(hitInfo);
        }

        public void SetHoverExit()
        {
            m_RunningGizmoMode = GizmoMode.None;
            m_RunningGizmoAxis = GizmoAxis.None;

            m_TranslateGizmo.SetHoverExit();
            m_ScaleGizmo.SetHoverExit();
            m_RotateGizmo.SetHoverExit();
        }

        public bool SetSelectGizmo(RaycastHit hitInfo, Ray pointerRay)
        {
            GizmoAxis gizmoAxis = GizmoAxis.None;
            m_TranslateOnPlane = false;

            //Find out selected gizmo mode and axis, there should only be one running mode
            if (m_TranslateGizmo.SetSelectGizmo(hitInfo, out gizmoAxis))
            {
                m_RunningGizmoMode = GizmoMode.Move;
                m_RunningGizmoAxis = gizmoAxis;

                if (m_RunningGizmoAxis == GizmoAxis.XY
                    || m_RunningGizmoAxis == GizmoAxis.XZ
                    || m_RunningGizmoAxis == GizmoAxis.YZ)
                {
                    m_TranslateOnPlane = true;
                }
            }
            else if (m_ScaleGizmo.SetSelectGizmo(hitInfo, out gizmoAxis))
            {
                m_RunningGizmoMode = GizmoMode.Scale;
                m_RunningGizmoAxis = gizmoAxis;
            }

            else if (m_RotateGizmo.SetSelectGizmo(hitInfo, out gizmoAxis))
            {
                m_RunningGizmoMode = GizmoMode.Rotate;
                m_RunningGizmoAxis = gizmoAxis;
            }
            else
            {
                return false;
            }

            //initialize the virtual working plane which will be used to calculate pointer movement
            m_CurrentPointerRay = pointerRay;
            m_OperationOrigin = m_TargetObject.transform.position;

            if (!m_TranslateOnPlane)
            {
                m_OperationPlaneNormal = m_CurrentPointerRay.origin - m_OperationOrigin;
                m_MousePositionOnOperationPlane = GeometryHelper.LinePlaneIntersect(m_CurrentPointerRay.origin, m_CurrentPointerRay.direction, m_OperationOrigin, m_OperationPlaneNormal);
                m_PreviousMousePositionOnOperationPlane = m_MousePositionOnOperationPlane;
            }

            m_TargetXAxis = m_TargetObject.transform.right;
            m_TargetYAxis = m_TargetObject.transform.up;
            m_TargetZAxis = m_TargetObject.transform.forward;

            m_ObjectWorkingAixs = Vector3.zero;

            //By the axis selected by the user, we should define which axis we should be working on.
            if (m_RunningGizmoMode == GizmoMode.Move)
            {
                if (gizmoAxis == GizmoAxis.X)
                    m_ObjectWorkingAixs = m_TargetXAxis;
                else if (gizmoAxis == GizmoAxis.Y)
                    m_ObjectWorkingAixs = m_TargetYAxis;
                else if (gizmoAxis == GizmoAxis.Z)
                    m_ObjectWorkingAixs = m_TargetZAxis;
                else if (gizmoAxis == GizmoAxis.XY)
                    m_OperationPlaneNormal = m_TargetZAxis;
                else if (gizmoAxis == GizmoAxis.YZ)
                    m_OperationPlaneNormal = m_TargetXAxis;
                else if (gizmoAxis == GizmoAxis.XZ)
                    m_OperationPlaneNormal = m_TargetYAxis;

                if (!m_TranslateOnPlane)
                {
                    m_ProjectedAxis = Vector3.ProjectOnPlane(m_ObjectWorkingAixs, m_OperationPlaneNormal).normalized;
                }
                else
                {
                    m_MousePositionOnOperationPlane = GeometryHelper.LinePlaneIntersect(m_CurrentPointerRay.origin, m_CurrentPointerRay.direction, m_OperationOrigin, m_OperationPlaneNormal);
                    m_PreviousMousePositionOnOperationPlane = m_MousePositionOnOperationPlane;
                }
            }
            else if (m_RunningGizmoMode == GizmoMode.Scale)
            {
                if (gizmoAxis == GizmoAxis.X)
                {
                    m_ObjectWorkingAixs = Vector3.right;
                    m_ProjectedAxis = Vector3.ProjectOnPlane(m_TargetXAxis, m_OperationPlaneNormal).normalized;
                }
                else if (gizmoAxis == GizmoAxis.Y)
                {
                    m_ObjectWorkingAixs = Vector3.up;
                    m_ProjectedAxis = Vector3.ProjectOnPlane(m_TargetYAxis, m_OperationPlaneNormal).normalized;
                }
                else if (gizmoAxis == GizmoAxis.Z)
                {
                    m_ObjectWorkingAixs = Vector3.forward;
                    m_ProjectedAxis = Vector3.ProjectOnPlane(m_TargetZAxis, m_OperationPlaneNormal).normalized;
                }
                else
                {
                    m_ObjectWorkingAixs = Vector3.right;
                    m_ProjectedAxis = Vector3.ProjectOnPlane(m_TargetXAxis, m_OperationPlaneNormal).normalized;
                }
            }
            else if (m_RunningGizmoMode == GizmoMode.Rotate)
            {
                if (gizmoAxis == GizmoAxis.X)
                    m_ObjectWorkingAixs = m_TargetXAxis;
                else if (gizmoAxis == GizmoAxis.Y)
                    m_ObjectWorkingAixs = m_TargetYAxis;
                else if (gizmoAxis == GizmoAxis.Z)
                    m_ObjectWorkingAixs = m_TargetZAxis;
                else if (gizmoAxis == GizmoAxis.Any)
                    m_ObjectWorkingAixs = m_OperationPlaneNormal;

                //Rotation is a ring that orthogonal with selected axis,
                //so the projected axis used to translate the mouse movement shall be parallel to the ring and orthogonal with selected axis and virtual plane normal
                if (gizmoAxis != GizmoAxis.Any)
                {
                    m_ProjectedAxis = Vector3.Cross(m_ObjectWorkingAixs, m_OperationPlaneNormal).normalized;
                }
                else
                {
                    //if it's any axis, we use the camera plane for calculation, so the projected axis shall be camera's right
                    m_ProjectedAxis = Camera.main.transform.right;
                }
            }

            return true;
        }

        public void ReleaseGizmo()
        {
            m_TranslateGizmo.ReleaseGizmo();
            m_RotateGizmo.ReleaseGizmo();
            m_ScaleGizmo.ReleaseGizmo();

            m_RunningGizmoMode = GizmoMode.None;
            m_RunningGizmoAxis = GizmoAxis.None;
        }

        public void UpdatePointerRay(Ray pointerRay)
        {
            m_CurrentPointerRay = pointerRay;
        }

        public LayerMask GetGizmoLayerMask()
        {
            return gameObject.layer;
        }

        private void Update()
        {
            if (!m_Enable)
                return;

            transform.position = m_TargetObject.transform.position;
            transform.rotation = m_TargetObject.transform.rotation;
            transform.localScale = Vector3.one * GetDistanceMultiplier() * m_ScaleFactor;

            if (m_RunningGizmoMode == GizmoMode.None)
            {
                return;
            }

            //Update mouse position
            m_MousePositionOnOperationPlane = GeometryHelper.LinePlaneIntersect(m_CurrentPointerRay.origin,
                                                                                                                                                    m_CurrentPointerRay.direction,
                                                                                                                                                    m_OperationOrigin,
                                                                                                                                                    m_OperationPlaneNormal);

            if (m_RunningGizmoMode == GizmoMode.Move)
            {
                if (!m_TranslateOnPlane)
                    OnUpdateTranslate();
                else
                    OnUpdateTranslateOnPlane();
            }
            else if (m_RunningGizmoMode == GizmoMode.Scale)
            {
                OnUpdateScale();
            }
            else if (m_RunningGizmoMode == GizmoMode.Rotate)
            {
                OnUpdateRotate();
            }

            m_PreviousMousePositionOnOperationPlane = m_MousePositionOnOperationPlane;
        }

        private void OnUpdateTranslate()
        {
            float amount = 0f;
            Vector3 movement = Vector3.zero;

            amount = VectorHelper.MagnitudeInDirection(m_MousePositionOnOperationPlane - m_PreviousMousePositionOnOperationPlane, m_ProjectedAxis);
            movement = m_ObjectWorkingAixs * amount;

            m_TargetObject.transform.Translate(movement, Space.World);
        }

        private void OnUpdateTranslateOnPlane()
        {
            Vector3 movement = m_MousePositionOnOperationPlane - m_PreviousMousePositionOnOperationPlane;
            m_TargetObject.transform.Translate(movement, Space.World);
        }

        private void OnUpdateScale()
        {
            float amount = 0f;
            Vector3 movement = Vector3.zero;

            amount = VectorHelper.MagnitudeInDirection(m_MousePositionOnOperationPlane - m_PreviousMousePositionOnOperationPlane, m_ProjectedAxis);

            if (m_RunningGizmoAxis != GizmoAxis.Any)
            {
                movement = m_ObjectWorkingAixs * amount;
                m_TargetObject.transform.localScale = m_TargetObject.transform.localScale + movement;
            }
            else
            {
                m_TargetObject.transform.localScale += m_TargetObject.transform.localScale * amount;
            }

            m_ScaleGizmo.SetScaleIndicator(m_RunningGizmoAxis, amount);
        }

        private void OnUpdateRotate()
        {
            float amount = 0f;

            amount = VectorHelper.MagnitudeInDirection(m_MousePositionOnOperationPlane - m_PreviousMousePositionOnOperationPlane, m_ProjectedAxis) * m_RotateSensitivity;

            m_TargetObject.transform.Rotate(m_ObjectWorkingAixs, amount, Space.World);
        }

        private float GetDistanceMultiplier()
        {
            if (Camera.main.orthographic)
                return Mathf.Max(.01f, Camera.current.orthographicSize * 2f);
            else
                return Mathf.Max(.01f, Mathf.Abs(VectorHelper.MagnitudeInDirection(transform.position - Camera.main.transform.position, Camera.main.transform.forward)));
        }
    }
}
