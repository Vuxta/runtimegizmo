﻿using UnityEngine;

namespace VuxtaStudio.GizmoRuntime.GizmoUI
{
    public abstract class ScaleGizmoUIBase : GizmoUIBase
    {
        public abstract void SetScaleIndicator(GizmoAxis gizmoAxis, float amount);
    }
}
