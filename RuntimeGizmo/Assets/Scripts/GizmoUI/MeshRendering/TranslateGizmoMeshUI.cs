﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace VuxtaStudio.GizmoRuntime.GizmoUI
{
    public class TranslateGizmoMeshUI : GizmoUIBase
    {
        [SerializeField]
        private Transform m_XAxisTransform;

        [SerializeField]
        private Transform m_YAxisTransform;

        [SerializeField]
        private Transform m_ZAxisTransform;

        [SerializeField]
        private MeshRenderer m_XAxisRender;

        [SerializeField]
        private MeshRenderer m_YAxisRender;

        [SerializeField]
        private MeshRenderer m_ZAxisRender;

        [SerializeField]
        private LineRenderer m_XAxisLineRenderer;

        [SerializeField]
        private LineRenderer m_YAxisLineRenderer;

        [SerializeField]
        private LineRenderer m_ZAxisLineRenderer;

        [SerializeField]
        private Collider m_XAxisCollider;

        [SerializeField]
        private Collider m_YAxisCollider;

        [SerializeField]
        private Collider m_ZAxisCollider;

        [SerializeField]
        private MeshRenderer m_XYPlaneRender;

        [SerializeField]
        private MeshRenderer m_YZPlaneRender;

        [SerializeField]
        private MeshRenderer m_XZPlaneRender;

        [SerializeField]
        private Collider m_XYPlaneCollider;

        [SerializeField]
        private Collider m_YZPlaneCollider;

        [SerializeField]
        private Collider m_XZPlaneCollider;

        [SerializeField]
        private Color m_XColor = new Color(1, 0, 0, 0.8f);

        [SerializeField]
        private Color m_YColor = new Color(0, 1, 0, 0.8f);

        [SerializeField]
        private Color m_ZColor = new Color(0, 0, 1, 0.8f);

        [SerializeField]
        private Color m_XYPlaneColor = new Color(0, 0, 1, 0.3f);

        [SerializeField]
        private Color m_XZPlaneColor = new Color(0, 1, 0, 0.3f);

        [SerializeField]
        private Color m_YZPlaneColor = new Color(1, 0, 0, 0.8f);

        [SerializeField]
        private Color m_SelectedColor = new Color(1, 1, 0, 0.8f);

        [SerializeField]
        private Color m_HoverColor = new Color(1, .75f, 0, 0.8f);

        private bool m_Enable = true;

        private void Awake()
        {
            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_XAxisLineRenderer.material.color = m_XColor;
            m_YAxisLineRenderer.material.color = m_YColor;
            m_ZAxisLineRenderer.material.color = m_ZColor;

            m_XYPlaneRender.material.color = m_XYPlaneColor;
            m_XZPlaneRender.material.color = m_XZPlaneColor;
            m_YZPlaneRender.material.color = m_YZPlaneColor;
        }

        public override bool IsEnable()
        {
            return m_Enable;
        }

        public override void SetEnable(bool onOff)
        {
            m_Enable = onOff;
            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_XAxisLineRenderer.material.color = m_XColor;
            m_YAxisLineRenderer.material.color = m_YColor;
            m_ZAxisLineRenderer.material.color = m_ZColor;

            m_XYPlaneRender.material.color = m_XYPlaneColor;
            m_XZPlaneRender.material.color = m_XZPlaneColor;
            m_YZPlaneRender.material.color = m_YZPlaneColor;

            m_XAxisRender.enabled = onOff;
            m_YAxisRender.enabled = onOff;
            m_ZAxisRender.enabled = onOff;

            m_XAxisLineRenderer.enabled = onOff;
            m_YAxisLineRenderer.enabled = onOff;
            m_ZAxisLineRenderer.enabled = onOff;

            m_XAxisCollider.enabled = onOff;
            m_YAxisCollider.enabled = onOff;
            m_ZAxisCollider.enabled = onOff;

            m_XYPlaneRender.enabled = onOff;
            m_XZPlaneRender.enabled = onOff;
            m_YZPlaneRender.enabled = onOff;

            m_XYPlaneCollider.enabled = onOff;
            m_YZPlaneCollider.enabled = onOff;
            m_XZPlaneCollider.enabled = onOff;
        }

        public override bool SetHover(RaycastHit hitInfo)
        {
            if (!m_Enable)
            {
                return false;
            }

            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_XAxisLineRenderer.material.color = m_XColor;
            m_YAxisLineRenderer.material.color = m_YColor;
            m_ZAxisLineRenderer.material.color = m_ZColor;

            m_XYPlaneRender.material.color = m_XYPlaneColor;
            m_XZPlaneRender.material.color = m_XZPlaneColor;
            m_YZPlaneRender.material.color = m_YZPlaneColor;

            if (hitInfo.collider == m_XAxisCollider)
            {
                m_XAxisRender.material.color = m_HoverColor;
                m_XAxisLineRenderer.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_YAxisCollider)
            {
                m_YAxisRender.material.color = m_HoverColor;
                m_YAxisLineRenderer.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_ZAxisCollider)
            {
                m_ZAxisRender.material.color = m_HoverColor;
                m_ZAxisLineRenderer.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_XYPlaneCollider)
            {
                m_XYPlaneRender.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_XZPlaneCollider)
            {
                m_XZPlaneRender.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_YZPlaneCollider)
            {
                m_YZPlaneRender.material.color = m_HoverColor;
                return true;
            }

            return false;
        }

        public override void SetHoverExit()
        {
            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor; ;

            m_XAxisLineRenderer.material.color = m_XColor;
            m_YAxisLineRenderer.material.color = m_YColor;
            m_ZAxisLineRenderer.material.color = m_ZColor;

            m_XYPlaneRender.material.color = m_XYPlaneColor;
            m_XZPlaneRender.material.color = m_XZPlaneColor;
            m_YZPlaneRender.material.color = m_YZPlaneColor;
        }

        public override bool SetSelectGizmo(RaycastHit hitInfo, out GizmoAxis gizmoAxis)
        {
            gizmoAxis = GizmoAxis.None;

            if (!m_Enable)
            {
                return false;
            }

            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_XAxisLineRenderer.material.color = m_XColor;
            m_YAxisLineRenderer.material.color = m_YColor;
            m_ZAxisLineRenderer.material.color = m_ZColor;

            m_XYPlaneRender.material.color = m_XYPlaneColor;
            m_XZPlaneRender.material.color = m_XZPlaneColor;
            m_YZPlaneRender.material.color = m_YZPlaneColor;

            if (hitInfo.collider == m_XAxisCollider)
            {
                m_XAxisRender.material.color = m_SelectedColor;
                m_XAxisLineRenderer.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.X;
                return true;
            }

            if (hitInfo.collider == m_YAxisCollider)
            {
                m_YAxisRender.material.color = m_SelectedColor;
                m_YAxisLineRenderer.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.Y;
                return true;
            }

            if (hitInfo.collider == m_ZAxisCollider)
            {
                m_ZAxisRender.material.color = m_SelectedColor;
                m_ZAxisLineRenderer.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.Z;
                return true;
            }

            if (hitInfo.collider == m_XYPlaneCollider)
            {
                m_XYPlaneRender.material.color = m_SelectedColor;
                m_XAxisRender.material.color = m_SelectedColor;
                m_YAxisRender.material.color = m_SelectedColor;
                m_XAxisLineRenderer.material.color = m_SelectedColor;
                m_YAxisLineRenderer.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.XY;
                return true;
            }

            if (hitInfo.collider == m_XZPlaneCollider)
            {
                m_XZPlaneRender.material.color = m_SelectedColor;
                m_XAxisRender.material.color = m_SelectedColor;
                m_ZAxisRender.material.color = m_SelectedColor;
                m_XAxisLineRenderer.material.color = m_SelectedColor;
                m_ZAxisLineRenderer.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.XZ;
                return true;
            }

            if (hitInfo.collider == m_YZPlaneCollider)
            {
                m_YZPlaneRender.material.color = m_SelectedColor;
                m_YAxisRender.material.color = m_SelectedColor;
                m_ZAxisRender.material.color = m_SelectedColor;
                m_YAxisLineRenderer.material.color = m_SelectedColor;
                m_ZAxisLineRenderer.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.YZ;
                return true;
            }

            return false;
        }

        private void Update()
        {
            m_XAxisLineRenderer.SetPosition(0, transform.position);
            m_XAxisLineRenderer.SetPosition(1, m_XAxisTransform.position);
            m_YAxisLineRenderer.SetPosition(0, transform.position);
            m_YAxisLineRenderer.SetPosition(1, m_YAxisTransform.position);
            m_ZAxisLineRenderer.SetPosition(0, transform.position);
            m_ZAxisLineRenderer.SetPosition(1, m_ZAxisTransform.position);
        }
    }
}