﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace VuxtaStudio.GizmoRuntime.GizmoUI
{
    public class RotateGizmoMeshUI : GizmoUIBase
    {
        [SerializeField]
        private Transform m_AnyAxisTransform;

        [SerializeField]
        private MeshRenderer m_XAxisRender;

        [SerializeField]
        private MeshRenderer m_YAxisRender;

        [SerializeField]
        private MeshRenderer m_ZAxisRender;

        [SerializeField]
        private Collider m_XAxisCollider;

        [SerializeField]
        private Collider m_YAxisCollider;

        [SerializeField]
        private Collider m_ZAxisCollider;

        [SerializeField]
        private MeshRenderer m_AnyAxisRender;

        [SerializeField]
        private Collider m_AnyAxisCollider;

        [SerializeField]
        private Color m_XColor = new Color(1, 0, 0, 0.8f);

        [SerializeField]
        private Color m_YColor = new Color(0, 1, 0, 0.8f);

        [SerializeField]
        private Color m_ZColor = new Color(0, 0, 1, 0.8f);

        [SerializeField]
        public Color m_AnyColor = new Color(0.7f, 0.7f, 0.7f, 0.8f);

        [SerializeField]
        private Color m_SelectedColor = new Color(1, 1, 0, 0.8f);

        [SerializeField]
        private Color m_HoverColor = new Color(1, .75f, 0, 0.8f);

        private bool m_Enable = true;
        private bool m_IsGizmoSelected = false;

        private void Awake()
        {
            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_AnyAxisRender.material.color = m_AnyColor;
        }

        public override bool IsEnable()
        {
            return m_Enable;
        }

        public override void SetEnable(bool onOff)
        {
            m_Enable = onOff;
            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_AnyAxisRender.material.color = m_AnyColor;

            m_XAxisRender.enabled = onOff;
            m_YAxisRender.enabled = onOff;
            m_ZAxisRender.enabled = onOff;

            m_XAxisCollider.enabled = onOff;
            m_YAxisCollider.enabled = onOff;
            m_ZAxisCollider.enabled = onOff;

            m_AnyAxisRender.enabled = onOff;
            m_AnyAxisCollider.enabled = onOff;
        }

        public override bool SetHover(RaycastHit hitInfo)
        {
            if (!m_Enable)
            {
                return false;
            }

            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_AnyAxisRender.material.color = m_AnyColor;

            if (hitInfo.collider == m_XAxisCollider)
            {
                m_XAxisRender.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_YAxisCollider)
            {
                m_YAxisRender.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_ZAxisCollider)
            {
                m_ZAxisRender.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_AnyAxisCollider)
            {
                m_AnyAxisRender.material.color = m_HoverColor;
            }

            return false;
        }

        public override void SetHoverExit()
        {
            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor; ;

            m_AnyAxisRender.material.color = m_AnyColor;
        }

        public override bool SetSelectGizmo(RaycastHit hitInfo, out GizmoAxis gizmoAxis)
        {
            gizmoAxis = GizmoAxis.None;

            if (!m_Enable)
            {
                return false;
            }

            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_AnyAxisRender.material.color = m_AnyColor;

            if (hitInfo.collider == m_XAxisCollider)
            {
                m_XAxisRender.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.X;
            }
            else if (hitInfo.collider == m_YAxisCollider)
            {
                m_YAxisRender.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.Y;
            }
            else if (hitInfo.collider == m_ZAxisCollider)
            {
                m_ZAxisRender.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.Z;
            }
            else if (hitInfo.collider == m_AnyAxisCollider)
            {
                m_AnyAxisRender.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.Any;
            }
            else
            {
                return false;
            }

            m_IsGizmoSelected = true;
            return true;
        }

        public override void ReleaseGizmo()
        {
            m_IsGizmoSelected = false;
        }

        private void Update()
        {
            if (!m_IsGizmoSelected)
            {
                m_AnyAxisTransform.transform.up = Camera.main.transform.position - m_AnyAxisTransform.transform.position;
            }
        }
    }
}
