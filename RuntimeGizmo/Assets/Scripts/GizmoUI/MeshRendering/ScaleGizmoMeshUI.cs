﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace VuxtaStudio.GizmoRuntime.GizmoUI
{
    public class ScaleGizmoMeshUI : ScaleGizmoUIBase
    {
        [SerializeField]
        private Transform m_XAxisTransform;

        [SerializeField]
        private Transform m_YAxisTransform;

        [SerializeField]
        private Transform m_ZAxisTransform;

        [SerializeField]
        private MeshRenderer m_XAxisRender;

        [SerializeField]
        private MeshRenderer m_YAxisRender;

        [SerializeField]
        private MeshRenderer m_ZAxisRender;

        [SerializeField]
        private LineRenderer m_XAxisLineRenderer;

        [SerializeField]
        private LineRenderer m_YAxisLineRenderer;

        [SerializeField]
        private LineRenderer m_ZAxisLineRenderer;

        [SerializeField]
        private Collider m_XAxisCollider;

        [SerializeField]
        private Collider m_YAxisCollider;

        [SerializeField]
        private Collider m_ZAxisCollider;

        [SerializeField]
        private MeshRenderer m_AnyAxisRender;

        [SerializeField]
        private Collider m_AnyAxisCollider;

        [SerializeField]
        private Color m_XColor = new Color(1, 0, 0, 0.8f);

        [SerializeField]
        private Color m_YColor = new Color(0, 1, 0, 0.8f);

        [SerializeField]
        private Color m_ZColor = new Color(0, 0, 1, 0.8f);

        [SerializeField]
        public Color m_AnyColor = new Color(0.7f, 0.7f, 0.7f, 0.8f);

        [SerializeField]
        private Color m_SelectedColor = new Color(1, 1, 0, 0.8f);

        [SerializeField]
        private Color m_HoverColor = new Color(1, .75f, 0, 0.8f);

        private bool m_Enable = true;
        private Vector3 m_XAxisStartPos;
        private Vector3 m_YAxisStartPos;
        private Vector3 m_ZAxisStartPos;
        private Vector3 m_XAxisDir;
        private Vector3 m_YAxisDir;
        private Vector3 m_ZAxisDir;
        private bool m_IsGizmoSelected = false;

        private void Awake()
        {
            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_XAxisLineRenderer.material.color = m_XColor;
            m_YAxisLineRenderer.material.color = m_YColor;
            m_ZAxisLineRenderer.material.color = m_ZColor;

            m_AnyAxisRender.material.color = m_AnyColor;
        }

        public override bool IsEnable()
        {
            return m_Enable;
        }

        public override void SetEnable(bool onOff)
        {
            m_Enable = onOff;
            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_XAxisLineRenderer.material.color = m_XColor;
            m_YAxisLineRenderer.material.color = m_YColor;
            m_ZAxisLineRenderer.material.color = m_ZColor;

            m_AnyAxisRender.material.color = m_AnyColor;

            m_XAxisRender.enabled = onOff;
            m_YAxisRender.enabled = onOff;
            m_ZAxisRender.enabled = onOff;

            m_XAxisLineRenderer.enabled = onOff;
            m_YAxisLineRenderer.enabled = onOff;
            m_ZAxisLineRenderer.enabled = onOff;

            m_XAxisCollider.enabled = onOff;
            m_YAxisCollider.enabled = onOff;
            m_ZAxisCollider.enabled = onOff;

            m_AnyAxisRender.enabled = onOff;
            m_AnyAxisCollider.enabled = onOff;
        }

        public override bool SetHover(RaycastHit hitInfo)
        {
            if (!m_Enable)
            {
                return false;
            }

            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_XAxisLineRenderer.material.color = m_XColor;
            m_YAxisLineRenderer.material.color = m_YColor;
            m_ZAxisLineRenderer.material.color = m_ZColor;

            m_AnyAxisRender.material.color = m_AnyColor;

            if (hitInfo.collider == m_XAxisCollider)
            {
                m_XAxisRender.material.color = m_HoverColor;
                m_XAxisLineRenderer.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_YAxisCollider)
            {
                m_YAxisRender.material.color = m_HoverColor;
                m_YAxisLineRenderer.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_ZAxisCollider)
            {
                m_ZAxisRender.material.color = m_HoverColor;
                m_ZAxisLineRenderer.material.color = m_HoverColor;
                return true;
            }

            if (hitInfo.collider == m_AnyAxisCollider)
            {
                m_AnyAxisRender.material.color = m_HoverColor;
            }

            return false;
        }

        public override void SetHoverExit()
        {
            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor; ;

            m_XAxisLineRenderer.material.color = m_XColor;
            m_YAxisLineRenderer.material.color = m_YColor;
            m_ZAxisLineRenderer.material.color = m_ZColor;

            m_AnyAxisRender.material.color = m_AnyColor;
        }

        public override bool SetSelectGizmo(RaycastHit hitInfo, out GizmoAxis gizmoAxis)
        {
            gizmoAxis = GizmoAxis.None;

            if (!m_Enable)
            {
                return false;
            }

            m_XAxisRender.material.color = m_XColor;
            m_YAxisRender.material.color = m_YColor;
            m_ZAxisRender.material.color = m_ZColor;

            m_XAxisLineRenderer.material.color = m_XColor;
            m_YAxisLineRenderer.material.color = m_YColor;
            m_ZAxisLineRenderer.material.color = m_ZColor;

            m_AnyAxisRender.material.color = m_AnyColor;

            if (hitInfo.collider == m_XAxisCollider)
            {
                m_XAxisRender.material.color = m_SelectedColor;
                m_XAxisLineRenderer.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.X;
            }
            else if (hitInfo.collider == m_YAxisCollider)
            {
                m_YAxisRender.material.color = m_SelectedColor;
                m_YAxisLineRenderer.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.Y;
            }
            else if (hitInfo.collider == m_ZAxisCollider)
            {
                m_ZAxisRender.material.color = m_SelectedColor;
                m_ZAxisLineRenderer.material.color = m_SelectedColor;
                gizmoAxis = GizmoAxis.Z;
            }
            else if (hitInfo.collider == m_AnyAxisCollider)
            {
                m_AnyAxisRender.material.color = m_SelectedColor;

                m_XAxisRender.material.color = m_SelectedColor;
                m_YAxisRender.material.color = m_SelectedColor;
                m_ZAxisRender.material.color = m_SelectedColor;

                m_XAxisLineRenderer.material.color = m_SelectedColor;
                m_YAxisLineRenderer.material.color = m_SelectedColor;
                m_ZAxisLineRenderer.material.color = m_SelectedColor;

                gizmoAxis = GizmoAxis.Any;
            }
            else
            {
                return false;
            }

            m_XAxisStartPos = m_XAxisTransform.position;
            m_YAxisStartPos = m_YAxisTransform.position;
            m_ZAxisStartPos = m_ZAxisTransform.position;
            m_XAxisDir = (m_XAxisStartPos - transform.position).normalized;
            m_YAxisDir = (m_YAxisStartPos - transform.position).normalized;
            m_ZAxisDir = (m_ZAxisStartPos - transform.position).normalized;
            m_IsGizmoSelected = true;

            return true;
        }

        public override void SetScaleIndicator(GizmoAxis gizmoAxis, float amount)
        {
            Vector3 movement = Vector3.zero;
            if (gizmoAxis == GizmoAxis.X)
            {
                movement = m_XAxisDir * amount;
                m_XAxisTransform.Translate(movement, Space.World);
            }
            else if (gizmoAxis == GizmoAxis.Y)
            {
                movement = m_YAxisDir * amount;
                m_YAxisTransform.Translate(movement, Space.World);
            }
            else if (gizmoAxis == GizmoAxis.Z)
            {
                movement = m_ZAxisDir * amount;
                m_ZAxisTransform.Translate(movement, Space.World);
            }
            else if (gizmoAxis == GizmoAxis.Any)
            {
                movement = m_XAxisDir * amount;
                m_XAxisTransform.Translate(movement, Space.World);
                movement = m_YAxisDir * amount;
                m_YAxisTransform.Translate(movement, Space.World);
                movement = m_ZAxisDir * amount;
                m_ZAxisTransform.Translate(movement, Space.World);
            }
        }

        public override void ReleaseGizmo()
        {
            if (m_IsGizmoSelected)
            {
                m_XAxisTransform.position = m_XAxisStartPos;
                m_YAxisTransform.position = m_YAxisStartPos;
                m_ZAxisTransform.position = m_ZAxisStartPos;
                m_IsGizmoSelected = false;
            }
        }

        private void Update()
        {
            m_XAxisLineRenderer.SetPosition(0, transform.position);
            m_XAxisLineRenderer.SetPosition(1, m_XAxisTransform.position);
            m_YAxisLineRenderer.SetPosition(0, transform.position);
            m_YAxisLineRenderer.SetPosition(1, m_YAxisTransform.position);
            m_ZAxisLineRenderer.SetPosition(0, transform.position);
            m_ZAxisLineRenderer.SetPosition(1, m_ZAxisTransform.position);
        }
    }
}
