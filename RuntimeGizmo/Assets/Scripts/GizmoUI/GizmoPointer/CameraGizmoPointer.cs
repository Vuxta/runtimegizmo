﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.GizmoRuntime.GizmoUI
{
    public class CameraGizmoPointer : GizmoPointerBase
    {
        private readonly MouseSelectObjectState mouseSelectObjectState = new MouseSelectObjectState();
        protected override SelectObjectStateBase m_MouseSelectObjectState
        {
            get
            {
                return mouseSelectObjectState;
            }
        }

        private readonly MouseHoverEnterState mouseHoverEnterState = new MouseHoverEnterState();
        protected override HoverEnterStateBase m_MouseHoverEnterState
        {
            get
            {
                return mouseHoverEnterState;
            }
        }

        private readonly MouseHoverState mouseHoverState = new MouseHoverState();
        protected override HoverStateBase m_MouseHoverState
        {
            get
            {
                return mouseHoverState;
            }
        }

        private readonly MouseSelectGizmoState mouseSelectGizmoState = new MouseSelectGizmoState();
        protected override SelectGizmoStateBase m_MouseSelectGizmoState
        {
            get
            {
                return mouseSelectGizmoState;
            }
        }
    }

    public class MouseSelectObjectState : SelectObjectStateBase
    {
        protected override Ray m_SelectObjectRay { get { return Camera.main.ScreenPointToRay(Input.mousePosition); } }
        protected override bool m_SelectObjectButtonDown { get { return Input.GetMouseButtonDown(0); } }
    }

    public class MouseHoverEnterState : HoverEnterStateBase
    {
        protected override Ray m_SelectObjectRay { get { return Camera.main.ScreenPointToRay(Input.mousePosition); } }
        protected override bool m_SelectObjectButtonDown { get { return Input.GetMouseButtonDown(0); } }
    }

    public class MouseHoverState : HoverStateBase
    {
        protected override Ray m_SelectObjectRay { get { return Camera.main.ScreenPointToRay(Input.mousePosition); } }
        protected override bool m_SelectObjectButtonDown { get { return Input.GetMouseButtonDown(0); } }
    }

    public class MouseSelectGizmoState : SelectGizmoStateBase
    {
        protected override Ray m_SelectObjectRay { get { return Camera.main.ScreenPointToRay(Input.mousePosition); } }
        protected override bool m_SelectButtonPressed { get { return Input.GetMouseButton(0); } }
    }
}
