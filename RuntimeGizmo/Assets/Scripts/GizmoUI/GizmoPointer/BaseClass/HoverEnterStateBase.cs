﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.GizmoRuntime.GizmoUI
{
    public abstract class HoverEnterStateBase : IState
    {
        protected LayerMask m_GizmoLayerMask;
        protected LayerMask m_ObjectLayerMask;
        protected Action<RaycastHit> m_OnHoverEnterCallback;
        protected Action<GameObject> m_OnObjectMouseDownCallback;

        protected abstract Ray m_SelectObjectRay { get; }
        protected abstract bool m_SelectObjectButtonDown { get; }

        public void Initialize(LayerMask gizmoLayerMask,
                                             LayerMask objectLayerMask,
                                             Action<RaycastHit> onHoverEnter,
                                             Action<GameObject> onObjectMouseDown)
        {
            m_GizmoLayerMask = gizmoLayerMask;
            m_ObjectLayerMask = objectLayerMask;
            m_OnHoverEnterCallback = onHoverEnter;
            m_OnObjectMouseDownCallback = onObjectMouseDown;
        }

        public virtual void OnStateEnter() { }

        public virtual void OnStateExit() { }

        public virtual void OnStateUpdate()
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(m_SelectObjectRay, out hitInfo, Mathf.Infinity, m_ObjectLayerMask)
                && m_SelectObjectButtonDown)
            {
                m_OnObjectMouseDownCallback(hitInfo.collider.gameObject);
                return;
            }

            if (Physics.Raycast(m_SelectObjectRay, out hitInfo, Mathf.Infinity, m_GizmoLayerMask))
            {
                m_OnHoverEnterCallback(hitInfo);
                return;
            }
        }
    }
}
