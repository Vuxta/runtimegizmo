﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.GizmoRuntime.GizmoUI
{
    public abstract class GizmoPointerBase : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_RuntimeGizmoPrefab;

        [SerializeField]
        private GizmoMode m_GizmoMode = GizmoMode.Move;

        protected abstract SelectObjectStateBase m_MouseSelectObjectState { get; }
        protected abstract HoverEnterStateBase m_MouseHoverEnterState { get; }
        protected abstract HoverStateBase m_MouseHoverState { get; }
        protected abstract SelectGizmoStateBase m_MouseSelectGizmoState { get; }

        private GameObject m_RuntimeGizmoObject;
        private RuntimeGizmo m_RuntimeGizmo;

        protected LayerMask m_ObjectLayerMask;
        protected LayerMask m_GizmoLayerMask;

        private StateMachine m_StateMachine;

        public void SetMode(GizmoMode gizmoMode)
        {
            if (gizmoMode == GizmoMode.None)
            {
                Debug.LogError("Do no set gizmo mode to none");
                return;
            }

            m_GizmoMode = gizmoMode;
        }

        private void Awake()
        {
            m_RuntimeGizmoObject = Instantiate(m_RuntimeGizmoPrefab);
            m_RuntimeGizmo = m_RuntimeGizmoObject.GetComponent<RuntimeGizmo>();
            m_GizmoLayerMask = 1 << m_RuntimeGizmo.GetGizmoLayerMask();
            m_ObjectLayerMask = ~m_GizmoLayerMask;

            m_MouseSelectObjectState.Initialize(m_ObjectLayerMask, OnSelectObject);
            m_MouseHoverEnterState.Initialize(m_GizmoLayerMask, m_ObjectLayerMask, OnHoverEnter, OnSelectObject);
            m_MouseHoverState.Initialize(m_GizmoLayerMask, OnHoverExit, OnSelectGizmo);
            m_MouseSelectGizmoState.Initialize(OnReleaseGizmo, OnUpdatePointerRay);

            m_StateMachine = new StateMachine();
            m_StateMachine.ChangeState(m_MouseSelectObjectState);
        }

        private void OnDisable()
        {
            m_RuntimeGizmo.SetGizmoDisable();
        }

        protected void OnUpdatePointerRay(Ray obj)
        {
            m_RuntimeGizmo.UpdatePointerRay(obj);
        }

        protected void OnReleaseGizmo()
        {
            m_RuntimeGizmo.ReleaseGizmo();
            m_StateMachine.ChangeState(m_MouseHoverState);
        }

        protected void OnSelectGizmo(RaycastHit obj, Ray pointerRay)
        {
            if (m_RuntimeGizmo.SetSelectGizmo(obj, pointerRay))
            {
                m_StateMachine.ChangeState(m_MouseSelectGizmoState);
            }
        }

        protected void OnHoverExit()
        {
            m_RuntimeGizmo.SetHoverExit();
            m_StateMachine.ChangeState(m_MouseHoverEnterState);
        }

        protected void OnHoverEnter(RaycastHit obj)
        {
            m_RuntimeGizmo.SetHover(obj);
            m_MouseHoverState.SetTarget(obj.collider);
            m_StateMachine.ChangeState(m_MouseHoverState);
        }

        protected void OnSelectObject(GameObject gameObject)
        {
            m_RuntimeGizmo.SetGizmoEnable(gameObject, m_GizmoMode);
            m_StateMachine.ChangeState(m_MouseHoverEnterState);
        }

        private void Update()
        {
            m_StateMachine.OnMachineUpdate();
        }
    }
}
