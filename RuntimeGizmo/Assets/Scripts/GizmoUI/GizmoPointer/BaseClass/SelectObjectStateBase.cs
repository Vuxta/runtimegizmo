﻿using System;
using UnityEngine;
using VuxtaStudio.Common.FiniteStateMachine;

namespace VuxtaStudio.GizmoRuntime.GizmoUI
{
    public abstract class SelectObjectStateBase : IState
    {
        protected LayerMask m_ObjectLayerMask;
        protected Action<GameObject> m_OnSelectCallback;

        protected abstract Ray m_SelectObjectRay { get; }
        protected abstract bool m_SelectObjectButtonDown { get; }


        public void Initialize(LayerMask layerMask,
                                            Action<GameObject> onSelectCallback)
        {
            m_ObjectLayerMask = layerMask;
            m_OnSelectCallback = onSelectCallback;
        }

        public virtual void OnStateEnter() { }

        public virtual void OnStateExit() { }

        public virtual void OnStateUpdate()
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(m_SelectObjectRay, out hitInfo, Mathf.Infinity, m_ObjectLayerMask)
                && m_SelectObjectButtonDown)
            {
                m_OnSelectCallback(hitInfo.collider.gameObject);
                return;
            }
        }
    }
}
